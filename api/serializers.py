from django.conf import settings
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.models import update_last_login
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from logs.models import Log
from posts.models import Post, Like


User = get_user_model()


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(CustomTokenObtainPairSerializer, cls).get_token(user)

        # Add custom attrs to the token
        token['username'] = user.username
        return token

    def validate(self, attrs):
        data = super().validate(attrs)

        if settings.SIMPLE_JWT_UPDATE_LAST_LOGIN:
            update_last_login(None, self.user)

        return data


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[password_validation.validate_password],
                                     style={'input_type': 'password'},)
    password2 = serializers.CharField(write_only=True, required=True, style={'input_type': 'password'},)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2', 'email', 'first_name', 'last_name')

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data.get('first_name', ''),
            last_name=validated_data.get('last_name', ''),
            password=validated_data['password']
        )

        return user


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'username', 'get_full_name')


class PostPreviewSerializer(serializers.ModelSerializer):
    author = serializers.StringRelatedField()
    likes_count = serializers.SerializerMethodField()

    def get_likes_count(self, post):
        likes_count = Like.objects.filter(post=post, ).count()
        return likes_count

    class Meta:
        model = Post
        fields = ('id', 'title', 'author', 'created', 'updated',  'likes_count', 'url')


class PostDetailSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    content = serializers.CharField(style={'base_template': 'textarea.html', 'rows': 10}, )
    likes_count = serializers.SerializerMethodField()

    def get_likes_count(self, post):
        likes_count = Like.objects.filter(post=post, ).count()
        return likes_count

    class Meta:
        model = Post

        fields = ('id', 'author', 'title', 'content', 'created', 'updated',  'likes_count',)
        read_only_fields = ('id', 'created', 'updated', )

    def validate_title(self, value):
        if len(value) < 5:
            raise serializers.ValidationError(_("The title length is too short!"))
        return value

    def validate(self, data):
        title = data.get("title", "")
        text = data.get("text", "")
        if all([title, text]) and len(title) + len(text) < 15:
            raise serializers.ValidationError(_("The title+text length is too short!"))
        return data


class PostLikeSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Like
        fields = ('id', 'user')


class LikesAnalyticsSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField()
    date = serializers.DateField()

    class Meta:
        model = Like
        fields = ('date', 'count')


class UserActionSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    last_login = serializers.SerializerMethodField()

    def get_last_login(self, *args, **kwargs):
        last_login = self.context.get('last_login')

        return last_login

    class Meta:
        model = Log
        fields = ('timestamp', 'user', 'url', 'description', 'last_login')
        read_only_fields = ('timestamp', 'user', 'url', 'description', 'last_login')
