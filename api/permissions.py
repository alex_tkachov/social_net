from rest_framework import permissions


class UserPermission(permissions.BasePermission):
    """User permissions"""
    def has_permission(self, request, view):
        if view.action in ['list', 'get_user_actions']:
            return request.user.is_authenticated and request.user.is_staff
        elif view.action == 'create':
            return True

        return False

    def has_object_permission(self, request, view, account):
        if view.action in ['retrieve', 'update', 'partial_update']:
            return account == request.user or request.user.is_staff
        return request.user.is_authenticated and request.user.is_staff


class PostPermission(permissions.BasePermission):
    """Post permissions"""
    def has_permission(self, request, view):
        if view.action in ['list', 'retrieve', 'create', 'update', 'partial_update', 'destroy', 'like_dislike',
                           'get_user_posts']:
            return request.user.is_authenticated

        return False

    def has_object_permission(self, request, view, obj):
        if view.action == 'retrieve':
            return request.user.is_authenticated
        elif view.action in ['update', 'partial_update']:
            return request.user.is_authenticated and obj.author == request.user
        elif view.action == 'destroy':
            return request.user.is_authenticated and (obj.author == request.user or request.user.is_staff)
        elif view.action == 'like_dislike':
            return request.user.is_authenticated and obj.author != request.user

        return False
