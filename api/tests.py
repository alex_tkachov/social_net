from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from rest_framework_simplejwt.tokens import RefreshToken

from posts.models import Post


User = get_user_model()


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class SocialNetApiTests(APITestCase):

    def setUp(self):
        self.username = 'test_user'
        self.password = 'test_password'
        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
        )
        self.user_tokens = get_tokens_for_user(self.user)
        self.user_access_token = f'Bearer {self.user_tokens["access"]}'

        self.another_user_username = 'another_user'
        self.another_user_password = 'another_password'
        self.another_user = User.objects.create_user(
            username=self.another_user_username,
            password=self.another_user_password,
        )
        self.another_user_tokens = get_tokens_for_user(self.another_user)
        self.another_user_access_token = f'Bearer {self.another_user_tokens["access"]}'

    def test_user_signup(self):
        url = reverse('register')
        data = {'username': 'user', 'email': 'user@example.com', 'password': 'useruser', 'password2': 'useruser'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_signin(self):
        url = reverse('token_obtain_pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'access')

    def test_create_post(self):
        url = reverse('post-list')
        data = {'title': 'title', 'content': 'content'}
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION=self.user_access_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_like_dislike_post(self):
        url = reverse('post-list')
        data = {'title': 'title', 'content': 'content'}
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION=self.user_access_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        post = Post.objects.get()
        url = reverse('post-like-dislike', args=[post.id])
        response = self.client.post(url, data, format='json', HTTP_AUTHORIZATION=self.user_access_token)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.post(url, format='json', HTTP_AUTHORIZATION=self.another_user_access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'like')

        response = self.client.post(url, format='json', HTTP_AUTHORIZATION=self.another_user_access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'dislike')
