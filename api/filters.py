import django_filters

from posts.models import Like


class LikesFilter(django_filters.FilterSet):
    date_from = django_filters.DateFilter(field_name='timestamp', lookup_expr='date__gte')
    date_to = django_filters.DateFilter(field_name='timestamp', lookup_expr='date__lte')

    class Meta:
        model = Like
        fields = ['timestamp', ]
