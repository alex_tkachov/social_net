from django.contrib.auth import get_user_model
from django.db.models import Count
from django.db.models.functions import TruncDate
from rest_framework import status, viewsets, permissions, generics
from rest_framework.decorators import api_view, action, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken, BlacklistedToken

from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView

from api.filters import LikesFilter
from api.permissions import UserPermission, PostPermission
from api.serializers import UserSerializer, PostPreviewSerializer, PostDetailSerializer, \
    PostLikeSerializer, RegisterSerializer, CustomTokenObtainPairSerializer, LikesAnalyticsSerializer, \
    UserActionSerializer
from logs.models import Log
from posts.models import Post, Like


class UserViewSet(viewsets.ModelViewSet):
    """User view"""
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = (UserPermission,)

    @action(methods=['get'], detail=True)
    def get_user_actions(self, request, *args, **kwargs):
        """Get user last_login and the last action"""
        user = self.get_object()
        last_action = Log.objects.filter(user=user).last()

        data = last_action and last_action or {'user': user}
        serializer = UserActionSerializer(data, initial=data, context={'last_login': user.last_login})

        return Response(serializer.data, status=status.HTTP_200_OK)


class PostViewSet(viewsets.ModelViewSet):
    """Post view"""
    queryset = Post.objects.order_by('-created')
    permission_classes = (PostPermission, )

    def get_serializer_class(self):
        if self.action == 'create':
            return PostDetailSerializer
        if self.action == 'list':
            return PostPreviewSerializer
        elif self.action == 'like_dislike':
            return PostLikeSerializer
        return PostDetailSerializer

    @action(detail=True, methods=['post'], )
    def like_dislike(self, request, *args, **kwargs):
        """like or dislike a post"""
        post = self.get_object()
        serializer = self.get_serializer(post, data=request.data)

        if request.method == 'POST':
            if serializer.is_valid():
                act = 'dislike'
                like, created = Like.like(post=post, **serializer.validated_data)
                if created:
                    act = 'like'

                return Response({'success': True, 'act': act}, status=status.HTTP_200_OK)

            return Response({'success': False, **serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'success': False}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @action(detail=False, methods=['get'])
    def get_user_posts(self, request):
        user = request.user
        queryset = Post.objects.filter(author=user)
        serializer = PostPreviewSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


# Auth ########################################################################
class CustomTokenObtainPairView(TokenObtainPairView):
    """User login view"""
    permission_classes = (AllowAny,)
    serializer_class = CustomTokenObtainPairSerializer


class RegisterView(generics.CreateAPIView):
    """User signup view"""
    queryset = get_user_model().objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class LogoutView(APIView):
    """User logout view"""
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class LogoutAllView(APIView):
    """Logout on all devices"""
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        tokens = OutstandingToken.objects.filter(user_id=request.user.id)
        for token in tokens:
            t, _ = BlacklistedToken.objects.get_or_create(token=token)

        return Response(status=status.HTTP_205_RESET_CONTENT)
# Auth end ###########################################################################


@api_view(['GET'])
@permission_classes([permissions.IsAdminUser])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'posts': reverse('post-list', request=request, format=format)
    })


class LikesAnalyticsView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = LikesAnalyticsSerializer
    filterset_class = LikesFilter

    def get_queryset(self):
        """
        Optionally restricts the returned likes
        by filtering against a `timestamp__date` query parameter in the URL.
        """
        queryset = Like.objects.annotate(
            date=TruncDate('timestamp')).values('date').annotate(count=(Count('timestamp__date')))

        return queryset
