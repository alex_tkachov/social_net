from django.contrib.auth.middleware import get_user
from django.utils.functional import SimpleLazyObject
from rest_framework.request import Request
from rest_framework_simplejwt.authentication import JWTAuthentication

from .models import Log


def get_user_jwt(request):
    """
    eliminates request.user always AnonymousUser in middlewares
    https://github.com/jpadilla/django-rest-framework-jwt/issues/45
    """
    user = get_user(request)
    if user.is_authenticated:
        return user
    try:
        user_jwt = JWTAuthentication().authenticate(Request(request))
        if user_jwt is not None:
            return user_jwt[0]
    except:
        pass
    return user


class LogMiddleware:
    """User actions tracking middleware"""
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        request.user = SimpleLazyObject(lambda: get_user_jwt(request))

        if request.method == "POST" and request.user.is_authenticated:
            Log.objects.create(
                user=request.user,
                url=request.path,
                description=view_func.__module__ + '.' + view_func.__name__
            )
        return None
