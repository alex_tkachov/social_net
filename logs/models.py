from django.conf import settings
from django.db import models


class Log(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    url = models.CharField(max_length=200)
    description = models.CharField(max_length=200)

    class Meta:
        ordering = ['timestamp']

    def __str__(self):
        return f'{self.timestamp}{self.user}{self.url}'
